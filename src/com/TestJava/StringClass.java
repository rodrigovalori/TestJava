package com.TestJava;

public class StringClass {

    public static void main(String[] args) {
        String name = "Rodrigo";

        System.out.println(name.toUpperCase());
        System.out.println(name.toLowerCase());
        System.out.println(name.charAt(0));
        System.out.println(name.contains("dri"));
        System.out.println(name.equals("RODRIGO"));
    }
}