package com.TestJava;

import java.time.LocalDate;

public class ReferenceTypes {

    public static void main(String[] args) {
        String name = "HelloWorld";

        System.out.println(name.toUpperCase());
        System.out.println(LocalDate.now().getMonth());
    }
}