package com.TestJava;

public class Arrays2 {

    public static void main(String[] args) {
        int [] numbers = {0, 1, 5, 3, 100, 19};

        int two = numbers[2];
        System.out.println(two);

        int lastNumber = numbers[numbers.length - 1];
        System.out.println(lastNumber);
    }
}