package com.TestJava;

public class Loops {

    public static void main(String[] args) {
        int[] numbers = {1, 3, 5, 7, 10};

        // Loops
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Enhanced Loops
        for (int number : numbers) {
            System.out.println(number);
        }

        // Bonus tip
        /*
        numbers.for
        numbers.fori
        number.forr
        */

    }
}