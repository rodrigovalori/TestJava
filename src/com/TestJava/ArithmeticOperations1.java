package com.TestJava;

public class ArithmeticOperations1 {

    public static void main(String[] args) {
        System.out.println(10 + 2);
        System.out.println(10 - 2);
        System.out.println(10 * 2);
        System.out.println(10 * 2 + 7);
        System.out.println(10 * (2 + 7));
        // Brackets, Orders, Division/Multiplication, Addition/Subtraction - BODMAS
        System.out.println(10 / 2);
        System.out.println(10 % 2);
        System.out.println(10 % 3);
    }
}