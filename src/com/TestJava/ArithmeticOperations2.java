package com.TestJava;

public class ArithmeticOperations2 {

    public static void main(String[] args) {
        // ++ (increments only the value one)
        int number0 = 0;
        number0 = number0 + 1;
        System.out.println(number0);

        // OR
        int zero = 0;
        zero++;
        System.out.println(zero);

        // OR
        zero += 1;
        System.out.println(zero);


        // -- (decrements only the value one)
        int number1 = 1;
        number1 = number1 - 1;
        System.out.println(number1);

        // OR
        int one = 1;
        one--;
        System.out.println(one);

        // OR
        one -= 1;
        System.out.println(one);
    }
}
