package com.TestJava;

public class ClassesAndObjects {

    public static void main(String[] args) {
        Lens lensOne = new Lens(
                "Sony",
                "85mm",
                true);

        Lens lensTwo = new Lens(
                "Sony",
                "30mm",
                true);

        Lens lensThree = new Lens(
                "Canon",
                "24-70mm",
                false);

        System.out.println("Lens 1");
        System.out.println(lensOne.brand);
        System.out.println(lensOne.focalLenght);
        System.out.println(lensOne.isPrime);
        System.out.println();

        System.out.println("Lens 2");
        System.out.println(lensTwo.brand);
        System.out.println(lensTwo.focalLenght);
        System.out.println(lensTwo.isPrime);
        System.out.println();

        System.out.println("Lens 3");
        System.out.println(lensThree.brand);
        System.out.println(lensThree.focalLenght);
        System.out.println(lensThree.isPrime);

    }

    static class Lens {
        String brand;
        String focalLenght;
        boolean isPrime;

        Lens(String brand,
             String focalLenght,
             boolean isPrime) {
            this.brand = brand;
            this.focalLenght = focalLenght;
            this.isPrime = isPrime;
        }
    }
}