package com.TestJava;

import java.time.LocalDate;

public class Methods2 {

    public static void main(String[] args) {
        Passport usPassport = new Passport(
                "12345",
                LocalDate.of(2025, 1, 1),
                "Rodrigo Valori");

        Passport ukPassport = new Passport(
                "54321",
                LocalDate.of(2026,12,6),
                "Sophia Zuppo");

        System.out.println(ukPassport.number);
        System.out.println(ukPassport.expiryDate);
        System.out.println(ukPassport.fullName);
        System.out.println();

        System.out.println(usPassport.number);
        System.out.println(usPassport.expiryDate);
        System.out.println(usPassport.fullName);

    }

    static class Passport {
        String number;
        LocalDate expiryDate;
        String fullName;

        public Passport(String number,
                        LocalDate expiryDate,
                        String fullName) {
            this.number = number;
            this.expiryDate = expiryDate;
            this.fullName = fullName;
        }
    }
}