package com.TestJava;

import java.util.Arrays;

public class Arrays1 {

    public static void main(String[] args){
        // int ARRAY
        int [] numbers1 = {1, 2};
        System.out.println(Arrays.toString(numbers1));
        // or
        int [] numbers = new int[2];
        numbers[0] = 1;
        numbers[1] = 2;
        System.out.println(Arrays.toString(numbers));

        // String ARRAY
        String[] strings1 = {"A", "B", "C"};
        System.out.println(Arrays.toString(strings1));
        // or
        String [] strings = new String[3];
        strings[0] = "A";
        strings[1] = "B";
        strings[2] = "C";
        System.out.println(Arrays.toString(strings));

        // boolean ARRAY
        boolean [] booleans1 = {true, false, (10 > 0), (10 > 0 && 0> 10)};
        System.out.println(Arrays.toString(booleans1));
        // or
        boolean [] booleans = new boolean[4];
        booleans[0] = true;
        booleans[1] = false;
        booleans[2] = (10 > 0);
        booleans[3] = (10 > 0 && 0 > 10);
        System.out.println(Arrays.toString(booleans));
    }
}