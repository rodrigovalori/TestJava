package com.TestJava;

public class WhileLoop {

    public static void main(String[] args) {
        int count = 1;

        // While Loop
        while (count <= 10) {
            System.out.println("Count " + count);
            count++;
        }

        // Do While Loop
        do {
            System.out.println("Count " + count);
            count++;
        }
        while (count <= 10);
    }
}
