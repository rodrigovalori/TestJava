package com.TestJava;

public class BreakAndContinue {

    public static void main(String[] args) {
        String[] names = {"A", "B", "C", "D"};

        // Break
        for (String name : names) {
            if (name.equals("C")) {
                break;
            }
            System.out.println(name);
        }

        // Continue
        for (String name : names) {
            if (name.equals("A")) {
                continue;
            }
            System.out.println(name);
        }
    }
}
