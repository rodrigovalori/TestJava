package com.TestJava;

import java.util.Arrays;

public class Methods1 {

    public static void main(String[] args) {
        char[] letters = {'A', 'A', 'B', 'C', 'D', 'D', 'D'};
        System.out.println(countOccurrences(letters, 'A'));
    }

    // searchLetter Method
    public static int countOccurrences(
            char[] letters, char searchLetter) {
        int count = 0;
        for (char letter : letters) {
            if (letter == searchLetter) {
                count++;
            }
        }
        return count;
    }
}