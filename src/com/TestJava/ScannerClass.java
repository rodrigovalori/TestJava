package com.TestJava;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Scanner;

public class ScannerClass {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Scanner String
        System.out.println("What's your name? ");
        String username = scanner.nextLine();
        System.out.println("Hello, " + username);

        // Scanner int
        System.out.println("How old are you? ");
        int age = scanner.nextInt();
        System.out.println("You were born in " + LocalDate.now().minusYears(age).getYear() + "!");
    }
}