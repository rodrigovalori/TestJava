package com.TestJava;

public class LogicalOperators {

    public static void main(String[] args) {
        boolean isAdult = false;
        boolean isStudent = true;
        boolean isAmigosCodeMember = true;

        System.out.println(isAdult);
        System.out.println(isStudent);

        System.out.println(isAdult && isStudent);
        // And: only true when both are true
        System.out.println(isAdult || isStudent);
        // Or: only false when both are false
        System.out.println((isAdult || !isStudent) && isAmigosCodeMember);
        // !: invert the value
        System.out.println((isAdult || isStudent) && isAmigosCodeMember);

        System.out.println(10 > 8 || 2 >= 2 && 10 < 0);
        // BODMAS
        System.out.println((10 > 8 || 2 >= 2) && 10 < 0);

    }
}