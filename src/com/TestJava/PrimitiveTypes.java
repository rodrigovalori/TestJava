package com.TestJava;

public class PrimitiveTypes {

    public static void main(String[] args) {
        byte Byte = -128;
        short Short = 1200;
        int Int = 1_202_010;
        long Long = 128_197_381_047L;
        float Float = 3.14F;
        double Double = 3.1415;
        boolean Boolean = true;
        char Char = 'a';

        System.out.println(Byte);
        System.out.println(Short);
        System.out.println(Int);
        System.out.println(Long);
        System.out.println(Float);
        System.out.println(Double);
        System.out.println(Boolean);
        System.out.println(Char);
    }
}