package com.TestJava;

public class ConcatenatingStrings {

    public static void main(String[] args) {
        String name = "Rodrigo";
        String lastName = "Valori";
        String fullName = name + " " + lastName;

        System.out.println(fullName);
        System.out.println(name.concat(lastName));
    }
}