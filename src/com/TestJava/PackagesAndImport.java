package com.TestJava;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class PackagesAndImport {

    public static void main(String[] args) {
        Date date = new Date();
        java.sql.Date date1 = new java.sql.Date(1);
        LocalDate localDate = LocalDate.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(date);
        System.out.println(localDate);
        System.out.println(localDateTime);
    }
}