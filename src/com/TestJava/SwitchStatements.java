package com.TestJava;

public class SwitchStatements {

    public static void main(String[] args) {
        String gender = "Unknown";
        if (gender.toUpperCase().equals("FEMALE")) {
            System.out.println("I'm a FEMALE!");

        } else if (gender.toUpperCase().equals("MALE")) {
            System.out.println("I'm a MALE!");

        } else if (gender.toUpperCase().equals("PREFER_NOT_TO_SAY")) {
            System.out.println("Prefer not to say!");

        } else {
            System.out.println("Unknown!");
        }

        // OR Switch Statements

        switch (gender.toUpperCase()) {
            case "FEMALE" :
                System.out.println("I'm a FEMALE!");
                break;
            case "MALE" :
                System.out.println("I'm a MALE!");
                break;
            case "PREFER_NOT_TO_SAY" :
                System.out.println("Prefer not to say!");
                break;
            default:
                System.out.println("Unknown!");
        }
    }
}