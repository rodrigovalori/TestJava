package com.TestJava;

public class IfStatements {

    public static void main(String[] args) {
        int age = 16;

        if (age >= 18) {
            System.out.println("Adult!");
        } else if (age >= 16 && age < 18) {
            System.out.println("Almost an adult!");
        } else {
            System.out.println("Not an adult!");
        }

        // OR Ternary Operator

        String message = (age >= 18) ? "Adult!" : "Not an adult!";
        System.out.println(message);
    }
}