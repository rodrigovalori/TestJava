package com.TestJava;

public class ComparisonOperators {

    public static void main(String[] args) {
        int rodrigoAge = 21;
        int sophiaAge = 20;
        boolean comparisonAge = rodrigoAge > sophiaAge;
        System.out.println(comparisonAge);
        // or
        System.out.println(rodrigoAge > sophiaAge);
        //Operators: <, >, <=, >=, ==, !=
    }
}
